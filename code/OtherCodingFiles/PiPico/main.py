from machine import *
import time

usleep = lambda x: time.sleep(x/1000000.0)

a = 0
b = 0

en2 = Pin(0, Pin.OUT)
en1 = Pin(1, Pin.OUT)

en2.off()
en1.on()

delta = 1
delta1 = 1
select = 0
NinoMode = 1

motor = PWM(Pin(2))
motor.freq(1000)

if NinoMode == 0:
    luz = PWM(Pin(4))
    luz.freq(60)
    luz.duty_u16(32767)
    luz.freq(30)
    luz.duty_u16(100)
else:
    luz = Pin(4, Pin.OUT)
    luz.low()

adc = ADC(Pin(26, Pin.IN))
i = 0

def handle_interrupt(Pin):
    global freq
    global b
    global i
    global delta
    delta = time.ticks_diff(time.ticks_us(), b) # compute time difference
    b = time.ticks_us()
    freq = int(1000000/(delta))
    i = 0
    
def handle_interrupt_nino(Pin):
    global freq
    global b
    global delta
    global select
    delta = time.ticks_diff(time.ticks_us(), b) # compute time difference
    b = time.ticks_us()
    
    usleep(int(delta/4*select)+int(delta/72))
    luz.high()
    usleep(33)
    luz.low()
    
    #usleep(65000)
    #usleep(666)
    
    
PIR_Interrupt=Pin(3,Pin.IN)
if NinoMode == 1:
    PIR_Interrupt.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt_nino)
else:
    PIR_Interrupt.irq(trigger=Pin.IRQ_RISING, handler=handle_interrupt)

    
i = 0
j = 0
k = 0
duty = 4000
delta_total = 0
motor.duty_u16(14000)

def nino_main():
    #global delta
    #global i
    global duty
    global select
    while True:
        #duty = adc.read_u16()
        #motor.duty_u16(65535 - duty)
        select = adc.read_u16()
        select = int(select/21845)
        
        '''
        i += 1
        if i >= 5000:
            print(delta, end = ' ')
            print(select, end = ' ')
            print()
            i = 0
        '''
            
            #luz.duty_u16(0)

def mainn():
    while True:
        #duty = adc.read_u16()
        #motor.duty_u16(65535 - duty)
        delta_total += delta
        k += 1
        if k >= j:
            delta = delta_total/k
            #print(delta)
            if(delta < 33330-17):
                print("desci", end=' ')
                duty = duty - 1
            elif(delta > 33336-17):
                print("subi", end=' ')
                duty = duty + 1
            print(duty)
            if(duty <= 3):
                duty = 3
            if(duty >= 65530):
                duty = 65530
            motor.duty_u16(duty)
            delta_total = 0
            k = 0
            j+=1
        
        #print(65535-duty, end=' ')
        #print(freq)
        # time.sleep(0.0001)
        '''
        if j >= 1000:
            print(duty, end=' ')
            print(freq, end=' ')
            print(delta, end=' ')
            #print(delta1)
            print()
            j = 0
        '''
        i += 1
        if i >= 50000:
            #print(65535 - duty)
            luz.duty_u16(0)



if NinoMode == 1:
    nino_main()
else:
    mainn()
